# Dijkstra Algorithm (traffic network)

在简化的交通网络中找到最快路线

## 工具

Jupyter Notebook

## 描述

通过运行时刻图，得到邻接表，并利用Dijkstra算法找到通往目的地的最快路线。

**Traffic Network Model Description.ipynb**：

描述简化后的交通网络模型。

**Adjacency List.ipynb**：

包含求解模型中邻接表的算法。

**Find Shortest Path.ipynb**：

通过Dijkstra算法找到最快路线。

### 输入   
运行时刻表

出发站

目的站

开始行程的时间

### 输出   
最快路线 

行程中抵达每一站的时间
